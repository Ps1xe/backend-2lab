import { UserEntity } from './user.entity';
import { CreateUserDto, UpdateUserDto } from './users.dto';
import { usersRepository } from './users.repository';

class UsersService {
  async getAllUsers(): Promise<UserEntity[]> {
 
    return usersRepository.findAll();
  }

  async getOneUser(id: UserEntity['id']): Promise<UserEntity> {
    return usersRepository.findOne(id);
  }

  async createUser(dto: CreateUserDto): Promise<UserEntity> {
    return usersRepository.createOne(dto);
  }

  async updateUser(
    id: UserEntity['id'],
    dto: UpdateUserDto
  ): Promise<UserEntity> {
    return usersRepository.updateOne(id, dto);
  }

  async deleteUser(id: UserEntity['id']): Promise<boolean> {
    return usersRepository.deleteOne(id);
  }
}

export const usersService = new UsersService();
