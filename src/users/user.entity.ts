export type UserEntity = {
  id: string;
  email: string;
  name: string;
  age?: number;
  phone?: string;
  updatedAt: string;
  createdAt: string;
};
